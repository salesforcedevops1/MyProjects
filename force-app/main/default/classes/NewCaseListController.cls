public class NewCaseListController {
    public List<Case> getNewCases(){
        List<Case> newCaseList = Database.query('SELECT Id, teste__c,  CaseNumber FROM Case WHERE Status = \'new\'');
        return newCaseList;
    }
}
